

# Python Code
data "archive_file" "zip" {
  type        = "zip"
  source_file = "dynamodb_lambda.py"
  output_path = "dynamodb_lambda.zip"
}

data "aws_caller_identity" "current" {}

locals {
    accountId = data.aws_caller_identity.current.account_id
}